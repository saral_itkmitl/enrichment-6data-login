<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use GuzzleHttp\Client;
use App\Http\Controllers\Redirect;
use Session;

class ProfileController extends Controller
{
    public function loginSSO(Request $request)

    {
        $email = $request->email;   //Get email form login page
        $password = $request->password;   //Get password form login page
        try {
				/*** Binding form **/
			$client = new Client(); //Create new request Object
			
			$currentProfile = self::GetCurrentProfile($email); //current profile from SSO
			$profile = new Profile();
			$profile->dob            = $currentProfile ['date_of_birth'];
			$profile->email          = $currentProfile ['email'];
			$profile->phone          = $currentProfile ['phone'];
			$profile->gender         = $currentProfile ['gender'];
			$profile->marital_status = $currentProfile ['marital_status'];
			$profile->monthly_income = $currentProfile ['monthly_income'];
			$SSOID					 = $currentProfile ['id'];
            	/*** Do login  **/
			$token  = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';
            $res = $client->post('https://sso.rabbit.co.th/v1/sso-auth/rewards/login',
                [
                    'headers' => [
                        //                                    'Content-Type' => 'application/json', //  it's got error 422
                        'Authorization' => 'Bearer ' . $token],
                    'form_params' => [
                        'credential' => $email,
                        'password' => $password],
                ]);

            /******* CheckSurveyDone ******/

            if (Profile::where('SSOID', '=', $SSOID)->exists()) {
                $statusToSSO = Profile::where('SSOID', '=', $SSOID)->first()->DataToSSO;
                if($statusToSSO == 'Y'){
                    return view('thanks');
                }
                return view('6data-form')->with('profile', $profile);
            } else {
                return view('6data-form')->with('profile', $profile);
            }
        }catch (\GuzzleHttp\Exception\RequestException $e) {
    //            Session::flash('message', 'ไม่สามารถเข้าสู่ระบบ กรุณาตรวจสอบอีเมลล์และรหัสผ่านของคุณ');
            return redirect('/')->with('message', 'ไม่สามารถเข้าสู่ระบบ กรุณาตรวจสอบอีเมลล์และรหัสผ่านของคุณ');
         }

    }


    public function saveProfile(Request $request)
    {	
	try{
        $cur_email = $request->cur_email;   //current email form enrichment
        $pre_email = $request->pre_email;   //previous email form login
        
        
        /*** Current Profile ****/
        
         $profile                  = new Profile(); 
         $profile->dob             = $request->dob;
         $profile->email           = $cur_email;
         $profile->phone           = $request->phone;
         $profile->gender          = $request->gender;
         $profile->marital_status  = $request->marital_status;
         $profile->monthly_income  = $request->monthly_income;
         $currentProfile = self::GetCurrentProfile($pre_email); //current profile from SSO
                     
         /*** Old Profile ****/
        
         $profile->odob            = $currentProfile ['date_of_birth'];
         $profile->oemail          = $currentProfile ['email'];
         $profile->ophone          = $currentProfile ['phone'];
         $profile->ogender         = $currentProfile ['gender'];
         $profile->omarital_status = $currentProfile ['marital_status'];
         $profile->omonthly_income = $currentProfile ['monthly_income'];
         $profile->traderid        = $currentProfile ['thor_id'];
         $profile->SSOID           = $currentProfile['id'];
         $profile->pre_email       = $pre_email;
        
		/*** If used to save to DB , Dont do again ***/
         if(Profile::where('SSOID', '=', $profile->SSOID)->doesntExist()){

            date_default_timezone_set('Asia/Bangkok');  //set timezone
            $cdt = date('Y-m-d H:i:s');     //create date
   
            /*** Get ID ***/
       

            /*** Data Description ****/
            $profile->data_source     = 'full-enrichment6data';
            $profile->GetDataDate     = $cdt;
            $profile->intPointsGiven  = '0';
            $profile->intlastestStep  = '1';
            $profile->DataToSSO       = 'B';
            $profile->PhoneToSSO      = 'B';
			$profile->toSSO           = 'B';
            $profile->utm_medium      = ''; //????
            $profile->utm_campaign    = ''; //????
            
            /*** Save to Database ****/
            $profile->save(); 
          }else{
			  //Try catch ??
             Profile::where("SSOID", $profile->SSOID)->update([
              "dob" => $request->dob,
              "phone" => $request->phone,
              "gender" => $request->gender,
              "marital_status" => $request->marital_status,
              "monthly_income" => $request->monthly_income,
              "email" => $pre_email
             ]);
			 
            }
        
        /**** Update to SSO*****/
        if($profile->phone != $profile->ophone){
            try{
                self::UpdatePhoneSSO($profile);
                return view('result')->with('profile',$profile);
            } catch (\Exception $e){
                Profile::where("email", $pre_email)->update(["PhoneToSSO" => 'N']);
				Session::flash('message', 'กรุณากรอกหมายเลขโทรศัพท์ที่มีอยู่จริง');
                return view('6data-form')->with('profile', $request); //error
            }
        }else {
            try{
                self::UpdateDataSSO($profile);
                Profile::where("SSOID", $profile->SSOID)->update(["DataToSSO" => 'Y']);
				Profile::where("SSOID", $profile->SSOID)->update(["toSSO" => 'Y']);
                return view('thanks');
            } catch (\Exception $e){
                Profile::where("SSOID", $profile->SSOID)->update(["DataToSSO" => 'N']);
				Profile::where("SSOID", $profile->SSOID)->update(["toSSO" => 'N']);
                return view('error'); //error
            }
        }
    }catch (\Exception $e){
		return redirect('/')->with('message', 'เกิดข้อผิดพลาดระหว่างบันทึกข้อมูล กรุณาลองใหม่อีกครั้ง');;
		}
	}

    public function VerifyOTP(Request $request){
		try{
		$SSOID = $request->SSOID; 
        /*********** Check Verification email and save to SSO ********/
        $verifyOTP = new Client();
//        $token = 'mGgtDgdEX4q48cKBk9X5DvPeS4mW8xsz';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        try {
              /***** stagging ******/
 //             $res = $verifyOTP->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/web/user/verify/update/phone',
              /***** production *****/
              $res = $verifyOTP->patch('https://sso.rabbit.co.th/v1/sso-user/web/user/verify/update/phone',
                             [
                                 'headers' => [
                                   'Authorization' => 'Bearer ' . $token ],
                                 'form_params' =>  [
                                   'phone' => $request->phone,
                                   'code' => $request->code,
                                 ],
                             ]);
            Profile::where("SSOID", $SSOID)->update(["PhoneToSSO" => 'Y']);
              try{
                  self::UpdateDataSSO($request);
                  Profile::where("SSOID", $SSOID)->update(["DataToSSO" => 'Y']);
				  Profile::where("SSOID", $SSOID)->update(["toSSO" => 'Y']);
				  return view('thanks');
              }catch (\Exception $e) {
                  Profile::where("SSOID", $SSOID)->update(["DataToSSO" => 'N']);
				  Profile::where("SSOID", $SSOID)->update(["toSSO" => 'N']);
				  return view('error');
              }

              return view('thanks');

        } catch (\Exception $e) {
			if(empty($request)){
				return redirect('/');
			}
			Session::flash('message', 'รหัส 4 หลักที่คุณกรอกไม่ถูกต้อง กรุณาตรวจสอบรหัสที่ระบบส่งไปยัง SMS ของคุณอีกครั้ง');
            return view('result')->with('profile', $request);
        }
	}catch (\Exception $e){
		return redirect('/');
		}
    }

    public function UpdatePhoneSSO($profile){
        $updatePhone = new Client();
        /***** Update 5 data except email to SSO *****/

 //       $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$profile->pre_email,    //staging
        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$profile->email,    //production
                            [
                                'form_params' =>  [
                                    'phone' => $profile->phone
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
        return $code;
    }

    public function UpdateDataSSO($profile){
        $updateData = new Client();
        /***** Update email to SSO(Backoffice) *****/

//			$res_backoffice = $updateData->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/backoffice/user/'.$profile->SSOID,    //staging
                $res_backoffice = $updateData->patch('https://sso.rabbit.co.th/v1/sso-user/backoffice/user/'.$profile->SSOID,    //production
            [
                'form_params' =>  [
                    'email' => $profile->email,
                    'date_of_birth' => $profile->dob,
                    'gender' => $profile->gender,
                    'marital_status' => $profile->marital_status,
                    'monthly_income' => $profile->monthly_income
                ],
            ]);
        $code = $res_backoffice->getStatusCode();
        return $code;
    }

    public function GetCurrentProfile($pre_email){
        $getProfile = new Client();
//        $token = 'qZqu0EK8aL09xTiirvSyC0cKr59jVCId';    //staging token
        $token = '5qfeQ2kqHmQT5CyzgQYpY8czNMU8HX6Z';    //production token
        /***** stagging ******/
//        $res = $getProfile->get('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards/users/email/'.$pre_email,
        /***** production *****/
        $res = $getProfile->get('https://sso.rabbit.co.th/v1/sso-user/rewards/users/email/'.$pre_email,
                       [
                           'headers' => [
                           'Authorization' => 'Bearer ' . $token ]
                       ]);

        $data = $res->getBody()->getContents();

        $json = json_decode($data,true);

        return $json['data'];
    }

    public function CheckEmail(Request $request)
    {
        $email  = $request->email;
        $client = new Client();
        $res    = $client->get('https://sso.rabbit.co.th/v1/sso-user/kiosk/user/email/status/'.$email);
        $data   = $res->getBody()->getContents();
        $json   = json_decode($data,true);
        return $json['data']['status']; //return 0 or 1
    }
	
	public function ResendOTP(Request $request){
        $updatePhone = new Client();
        /***** Update 5 data except email to SSO *****/

 //       $res_phone = $updatePhone->patch('https://staging-sso-sso.rabbitinternet.com/v1/sso-user/rewards-campaign/users/by-email/'.$profile->pre_email,    //staging
        $res_phone = $updatePhone->patch('https://sso.rabbit.co.th/v1/sso-user/rewards-campaign/users/by-email/'.$request->email,    //production
                            [
                                'form_params' =>  [
                                    'phone' => $request->phone
                                   ],
                            ]);

        $code = $res_phone->getStatusCode();
        return $code;
	
}
