<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Eloquent;
class Profile extends Model
{
    const CREATED_AT = 'CreatedDate';
    const UPDATED_AT = 'UpdatedDate';
    protected $table = 'dataenrich_6demo';  // DatabaseTableName

    protected $fillable = ['data_source',
                           'email',
                           'traderid',
                           'SSOID',
                           'oemail',
                           'odob',
                           'ogender',
                           'omarital_status',
                           'omonthly_income',
                           'ophone',
                           'GetDataDate',
                           'dob',
                           'gender',
                           'marital_status',
                           'monthly_income',
                           'phone',
                           'intPointsGiven',
                           'intlastestStep',
                           'toSSO',
                           'CreatedDate',
                           'UpdatedDate',
                           'CreatedBy',
                           'utm_medium',
                           'utm_campaign',
                           'pre_email',
                          ];
    
    
}

