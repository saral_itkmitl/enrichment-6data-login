<?php

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Profile;


Route::get('/test', function () {
    $email = 'saralk@rabbit.co.th';
    $statusSSO = Profile::where('email', '=', $email)->first();
    return $statusSSO;


});

/////////////////////////////////////////////////////////


Route::get('/', function () {
    return view('login');
});


Route::get('/login','ProfileController@loginSSO');
Route::post('/login','ProfileController@loginSSO');

Route::get('/saveProfile','ProfileController@saveProfile');
Route::post('/saveProfile','ProfileController@saveProfile');

Route::get('/patchProfile','ProfileController@patchProfile');
Route::patch('/patchProfile','ProfileController@patchProfile');

Route::get('/CheckEmail','ProfileController@CheckEmail');
Route::post('/CheckEmail','ProfileController@CheckEmail');


Route::get('/VerifyOTP','ProfileController@VerifyOTP');
Route::post('/VerifyOTP','ProfileController@VerifyOTP');

Route::get('/ResendOTP','ProfileController@UpdatePhoneSSO');
Route::post('/ResendOTP','ProfileController@UpdatePhoneSSO');

