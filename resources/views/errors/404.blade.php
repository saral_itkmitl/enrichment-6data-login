@extends('layouts.app')

@section('content')
<div class="container">
      <div class="row">
          <div class="twleve columns" style="text-align:center; padding-top:3%; color:white;">
              <h3 style="color:#ffffff; font-weight:400;">ขออภัยค่ะ</h3>
              <div style="color:#fff; font-size:1.5em; line-height:1.2em;">เราไม่พบหน้าที่คุณต้องการ</div><br>
              <button class="button-primary" onclick="window.location='https://rabbitrewards.co.th'">เข้าสู่เว็บไซต์ Rabbit Rewards</button><br>
              <img src="images/rabbit_nottarget.png" class="u-max-full-width" style="min-width:380px; max-width: 60%;" />
          </div>
      </div>
  </div>
@endsection
