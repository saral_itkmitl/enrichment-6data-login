@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row"  style="text-align:center;">
      <h4 style="border:0;padding-top:30px;color:#000;font-weight:500;">
        เข้าสู่ระบบเพื่อเตรียมรับรางวัล !</h4>
        {{-- <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center; padding-top:25px; "> --}}

        {{-- <input  type="email" class="" style="border:1px;" name="email"  required autofocus>
        <input  type="password" class="" style="border:1px;margin-left:5px;margin-bottom:25px;" name="password"  required>

        <br>

        <button type="submit" class="btn btn-primary"  >
        เข้าสู่ระบบ
        </button>
        <br>
        --}}
  <form class="form-signin mg-btm" method="POST" action="{{ url('/login') }}">
  @csrf

  <div class="col-12">

    @if(Session::has('message'))
        <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>
    @endif
    <div class="modal-dialog" style="margin-bottom:0">
      <div class="modal-content" style="background-color: #fff;">
        <div class="panel-heading" style="text-align:center;">
          <h4 style="text-align:center;; ">RabbitRewards Survey</h4>
        </div>
        <div class="panel-body">



            <div class="form-group" style="text-align:center;">
              <input class="form-control six-columns" style="text-align:center;" placeholder="อีเมล" name="email" type="email" autofocus required>
            </div>
            <div class="form-group">
              <input class="form-control" style="text-align:center;" placeholder="รหัสผ่าน" name="password" type="password" required>
            </div>

            <!-- Change this to a button or input when using this as a form -->
            <div class="row" style="text-align:center;padding-top:15px;">
              <button type="submit"  class="btn btn-warning" style="color: #333;">เข้าสู่ระบบ</button>
            </div>
            <a class="btn btn-link" style="color:black;padding-top:20px;" href="https://rewards-sso.rabbit.co.th/users/forget_password">
              ลืมรหัสผ่าน?
            </a>


        </div>
      </div>
    </div>

  </div>
  </form>

</div>
{{-- <div class="three columns" style="height:1px;">&nbsp;</div> --}}

</div>
@endsection
