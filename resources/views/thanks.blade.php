@extends('layouts.app')
@section('content')

<div class="container">
            <div class="row">
                    <div class="three columns" style="height:1px;">&nbsp;</div>
                    <div class="six columns" style="text-align:center; color:#FFFFFF; line-height:1.8em;">
                        <br>
                        <span style="font-size:2.6em;  line-height:1.8em; ">ขอบคุณค่ะ</span><br>
                        <span style="font-size:1.5em;  line-height:1.6em; ">ข้อมูลส่วนตัวของคุณได้ถูกบันทึกแล้ว</span><br>
                        <span style="font-size:1.5em;  line-height:1.6em;   color:black;">เตรียมรับเซอร์ไพรส์ในเดือนเกิดได้เลย!</span>
                    </div>
                    <div class="three" style="height:1px;">&nbsp;</div>

                </div>
                <div class="row">
                    <div class="three columns" style="height:1px;">&nbsp;</div>
                    <div class="six columns" style="text-align:center; padding-top:15px; color:#808080;"><br>
                        <button class="button-primary" onclick="window.location='https://id.rabbit.co.th/login?utm_source=CampaignPage&utm_medium=referral&utm_campaign=Citibank_19APR'" target="_blank">ดูบัญชี</button>
                        <br>
                        <img src="images/survey/bg-ptai-check-ac.jpg" style="max-width:320px; box-sizing:border-box;"  class="u-max-full-width" >
                    </div>
                    <div class="three columns" style="height:1px;">&nbsp;</div>
                </div>
        </div>

@endsection
