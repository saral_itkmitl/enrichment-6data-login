@extends('layouts.app')
@section('content')

  <div class="container">

    <div class="row">
    <div class="two columns" style="height:1px;">&nbsp;</div>
    <div class="eight columns" style="text-align:left; padding-top:15px; ">
        
        @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>
        @endif
        
    <form action="{{ url('/VerifyOTP') }}" method="post" id="optionForm" class="form-group">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">
      
          <input type="hidden" name="phone" value="{{$profile->phone}}" />
          <input type="hidden" name="email" value="{{$profile->email}}" />
          <input type="hidden" name="dob" value="{{$profile->dob}}" />
          <input type="hidden" name="gender" value="{{$profile->gender}}" />
          <input type="hidden" name="marital_status" value="{{$profile->marital_status}}" />
          <input type="hidden" name="monthly_income" value="{{$profile->monthly_income}}" />
          <input type="hidden" name="SSOID" value="{{$profile->SSOID}}" />
		 <div class="modal-content" style="text-align:center;" >
			<div class="panel-body" style="border:1px;" style="padding-bottom:5px;text-align:center;">
			  <label class="control-label" style="text-align:center;padding-bottom:10px;" for="code">ระบบได้ส่ง SMS ไปยังเบอร์ {{$profile->phone}} แล้ว กรุณากรอกรหัสที่ได้รับในช่องด้านล่าง</label>
			  <input type="tel" style="text-align:center;padding-top:10px;" name="code"  onkeypress='return event.charCode >= 43 && event.charCode <= 57'  maxlength="4" title="กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง"  placeholder="กรุณากรอกรหัสที่ได้รับ 4 หลัก"   required data-errormessage-value-missing="กรุณากรอกรหัสที่ได้รับ 4 หลัก" autofocus/>
			
			</div>
			 <button class="btn btn-link" onclick="myFunction()" style="text-decoration: underline;color:black;margin-bottom:10px;">ส่งSMSอีกครั้ง</button>
			
        
		</div>

	
  </div>
<div class="two columns" style="height:1px;">&nbsp;</div>
        </div>
		
      <div class="row">
        <div class="three columns" style="height:1px;">&nbsp;</div>
        <div class="six columns" style="text-align:center;"><button type="submit" style="margin-top:25px;" id="button-sub" class="button-primary">ส่งข้อมูล</button><br>
          
		  <img src="images/survey/bg-survey.jpg" style="max-width:320px;">
        </div>
        <div class="three columns" style="height:1px;">&nbsp;</div>
      </div>
    </form>
	</div>
@endsection
