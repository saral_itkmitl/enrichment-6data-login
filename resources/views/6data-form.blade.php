@extends('layouts.form-layout')

@section('content')

<!--FORM-->
<div class="container">
    <div class="row">

                <div class="two columns" style="height:1px;">&nbsp;</div>
                  <div class="eight columns" style="text-align:center; padding-top:15px; font-size:1.3em; color:white; ">
                    <!--<img src="images/survey/icn-100p.png" style="max-height:70px;" >-->
                    เตรียมรับเซอร์ไพรส์ในเดือนเกิด
                    <br>
                    <span style="color:#333333;">เพียงอัพเดตข้อมูลส่วนตัวของคุณ</span></div>
                <div class="two columns" style="height:1px;">&nbsp;</div>
            </div>
            <div class="row">
               
                <div class="two columns" style="height:1px;">&nbsp;</div>
                <div class="eight columns" style="text-align:left; padding-top:15px; ">
				@if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class', 'alert-warning') }}">{{ Session::get('message') }}</p>

        @endif
                    <ol>
                         <form action="{{ url('/saveProfile') }}" method="post" id="optionForm" class="form-group">
                             @csrf
                        <input type="hidden" name="pre_email" value="{{$profile->email}}" />

                        <!-- 1 DOB-->
                        <li class="box-q" style="border:1px;">
                            <label class="control-label" for="dob">วันเกิดของคุณ (ค.ศ.)<span style="color:red;"> *</span></label>
                            <div class="controls">
                                <!-- -->
                                <input value="{{$profile->dob}}" type="text" class="form-control" name="dob" placeholder="yyyy-mm-dd" id="datetimepicker1" autocomplete="off" title="กรุณาเลือกวันเดือนปีเกิดของคุณ" required data-errormessage-value-missing="กรุณาเลือกวันเดือนปีเกิดของคุณ" />

                            </div>
                        </li>

                        <!-- 2 email-->

                        <li class="box-q" style="border:1px;">
                            <label class="control-label" for="email">อีเมลล์ของคุณ<span style="color:red;"> *</span></label>
                            <input value="{{$profile->email}}" type="email" name="cur_email" id="checkEmail" class="u-full-width"  maxlength="50" title="กรุณากรอกอีเมลล์ให้ถูกต้อง"  placeholder="email@example.com" autocomplete="off" autocorrect="off" autocapitalize="off"  required data-errormessage-value-missing="กรุณากรอกอีเมลล์ของคุณ"/>
                            <span id="user-result"></span>
                        </li>


                         <!-- 3 phone-->
                        <li class="box-q" style="border:1px;">
                            <label class="control-label" for="phone">เบอร์โทรศัพท์ของคุณ<span style="color:red;"> *</span></label>
                            <input value="{{$profile->phone}}" type="tel" name="phone"  class="u-full-width" onkeypress='return event.charCode >= 43 && event.charCode <= 57' minlength="10" maxlength="10" title="กรุณากรอกเบอร์โทรศัพท์ให้ถูกต้อง"  placeholder="กรุณากรอกเบอร์โทรศัพท์ 10 หลัก"   required data-errormessage-value-missing="กรุณากรอกเบอร์โทรศัพท์ของคุณ"/>
                            <span id="user-result"></span>
                        </li>

                       
                        <!-- 4 GENDER -->
                        <li class="box-q" style="border:1px;">
                            <label class="control-label" for="gender">เพศของคุณ<span style="color:red;"> *</span></label><br>
                            @if($profile->gender == 'male')
                            <input type="radio" checked="checked" name="gender" value="male" id="q2-a" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" required data-errormessage-value-missing="กรุณาเลือกเพศของคุณ" />
                            @else
                            <input type="radio" name="gender" value="male" id="q2-a" title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ" required data-errormessage-value-missing="กรุณาเลือกเพศของคุณ" />
                            @endif
                            <label class="label-body" for="q2-a">ชาย</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            @if($profile->gender == 'female')
                            <input type="radio" checked="checked" name="gender" value="female" id="q2-b">
                            @else
                            <input type="radio" name="gender" value="female" id="q2-b">
                            @endif
                            <label class="label-body" for="q2-b">หญิง</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <!--
                        <input type="radio" name="gender"  value="other" id="q2-c" >
                            <label class="label-body" for="q2-c">อื่นๆ</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            -->
                        </li>

                        <!-- 5 Marital Status -->
                        <li class="box-q" style="border:1px;">
                            <label class="control-label" for="marital_status">สถานะของคุณ<span style="color:red;"> *</span></label><br>
                            @if($profile->marital_status == 'single')
                            <input type="radio" checked="checked" name="marital_status" value="single" id="q3-a"  title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ"  required data-errormessage-value-missing="กรุณาเลือกสถานะของคุณ"/>
                            @else
                            <input type="radio" name="marital_status" value="single" id="q3-a"  title="กรุณาตอบแบบสอบถามให้ครบถ้วนค่ะ"  required data-errormessage-value-missing="กรุณาเลือกสถานะของคุณ"/>
                            @endif
                            <label class="label-body" for="q3-a">โสด</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            @if($profile->marital_status == 'married')
                            <input type="radio" checked="checked" name="marital_status" value="married" id="q3-b" />
                            @else
                            <input type="radio" name="marital_status" value="married" id="q3-b" />
                            @endif
                            <label class="label-body" for="q3-b">สมรส</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </li>

                        <!-- 6 income -->
                        <li class="box-q" style="border:1px;">

                            <label class="control-label" for="monthly_income">รายได้ต่อเดือนของคุณ<span style="color:red;"> *</span></label><br>
                            <select id="q4" class="u-full-width" name="monthly_income" title="กรุณาระบุรายได้ต่อเดือนของคุณ" required data-errormessage-value-missing="กรุณาเลือกช่วงรายได้ต่อเดือนของคุณ" >

                            <option value="">รายได้ต่อเดือน</option>
                            @if($profile->monthly_income == 'less_than_10000')
                            <option value="less_than_10000" selected="selected" required>น้อยกว่า 10,000 บาท</option>
                            @else
                            <option value="less_than_10000" required>น้อยกว่า 10,000 บาท</option>
                            @endif
                            @if($profile->monthly_income == '10001_20000')  
                            <option value="10001_20000" selected="selected">10,001 - 20,000 บาท</option>
                            @else
                            <option value="10001_20000">10,001 - 20,000 บาท</option>
                            @endif
                            @if($profile->monthly_income == '20001_35000') 
                            <option value="20001_35000" selected="selected">20,001 - 35,000 บาท</option>
                            @else
                            <option value="20001_35000">20,001 - 35,000 บาท</option>
                            @endif
                            @if($profile->monthly_income == '35001_50000')
                            <option value="35001_50000" selected="selected">35,001 - 50,000 บาท</option>
                            @else
                            <option value="35001_50000">35,001 - 50,000 บาท</option>  
                            @endif
                            @if($profile->monthly_income == '50001_70000')
                            <option value="50001_70000" selected="selected">50,001 - 70,000 บาท</option>
                            @else
                            <option value="50001_70000">50,001 - 70,000 บาท</option>
                            @endif
                            @if($profile->monthly_income == '70001_100000')
                            <option value="70001_100000" selected="selected">70,000 - 100,000 บาท</option>
                            @else
                            <option value="70001_100000">70,000 - 100,000 บาท</option>
                            @endif
                            @if($profile->monthly_income == 'above_100000')
                            <option value="above_100000" selected="selected">มากกว่า 100,000 บาท</option>
                            @else
                            <option value="above_100000">มากกว่า 100,000 บาท</option>  
                            @endif
                            </select>
                        </li>

                    </ol>
   
            </div>
                    <div class="two columns" style="height:1px;">&nbsp;</div>
                </div>
            <div class="row">
                <div class="three columns" style="height:1px;">&nbsp;</div>
                <div class="six columns" style="text-align:center;"><button type="submit" id="button-sub" class="button-primary">ส่งข้อมูล</button><br>
                    <img src="images/survey/bg-survey.jpg" style="max-width:320px;">
                </div>
                <div class="three columns" style="height:1px;">&nbsp;</div>
            </div>
            </form>
</div>




@endsection
