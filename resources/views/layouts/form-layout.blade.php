<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta property="og:image" content="https://rewards.rabbit.co.th/images/favicon/rabbit/mstile-144x144.png" />
    <meta name="viewport" content="initial-scale=1, maximum-scale=1">
    <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta charset="utf-8">
    <title>Rabbit Rewards Survey</title>
    <meta name="description" content="Design page by Wiseperzy">
    <meta name="author" content="RabbitRewards Co., Ltd.">



    <!-- Favicon
–––––––––––––––––––––––––––––––––––––––––––––––––– -->

    <link rel="apple-touch-icon" sizes="57x57" href="/images/favicon/rabbit/xapple-touch-icon-57x57.png.pagespeed.ic.Kv83gfAkcy.webp">
    <link rel="apple-touch-icon" sizes="60x60" href="/images/favicon/rabbit/xapple-touch-icon-60x60.png.pagespeed.ic.wPF2y-gMyX.webp">
    <link rel="apple-touch-icon" sizes="72x72" href="/images/favicon/rabbit/xapple-touch-icon-72x72.png.pagespeed.ic.3aQDL3Wg7D.webp">
    <link rel="apple-touch-icon" sizes="76x76" href="/images/favicon/rabbit/xapple-touch-icon-76x76.png.pagespeed.ic.5i7j7lfwsk.webp">
    <link rel="apple-touch-icon" sizes="114x114" href="/images/favicon/rabbit/xapple-touch-icon-114x114.png.pagespeed.ic.lJGztyxuV9.webp">
    <link rel="apple-touch-icon" sizes="120x120" href="/images/favicon/rabbit/xapple-touch-icon-120x120.png.pagespeed.ic.x78ZT_IWsx.webp">
    <link rel="apple-touch-icon" sizes="144x144" href="/images/favicon/rabbit/xapple-touch-icon-144x144.png.pagespeed.ic.fEKJC-Sl7t.webp">
    <link rel="apple-touch-icon" sizes="152x152" href="/images/favicon/rabbit/xapple-touch-icon-152x152.png.pagespeed.ic.AGY-o9Zeb9.webp">
    <link rel="apple-touch-icon" sizes="180x180" href="/images/favicon/rabbit/xapple-touch-icon-180x180.png.pagespeed.ic.Cn7F5T_Fxs.webp">
    <link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-32x32.png.pagespeed.ic.b_qDo3k2Eq.webp" sizes="32x32">
    <link rel="icon" type="image/png" href="/images/favicon/rabbit/xandroid-chrome-192x192.png.pagespeed.ic.JTL1IbgwT-.webp" sizes="192x192">
    <link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-96x96.png.pagespeed.ic.g5kxEtDa8A.webp" sizes="96x96">
    <link rel="icon" type="image/png" href="/images/favicon/rabbit/xfavicon-16x16.png.pagespeed.ic.VZ99nloZzF.webp" sizes="16x16">
    <link rel="manifest" href="/images/favicon/rabbit/manifest.json">

    <!-- -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/css/bootstrap-datetimepicker.min.css" />


    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>
    

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.37/js/bootstrap-datetimepicker.min.js"></script>

    <script src="js/civem.js"></script>


    <!-- FUNCTION DATETIME -->
    <script>
        /*
    function submitForm(action,value)
    {
        document.getElementById('optionForm').action = action;
        document.getElementById('salary').value = value;
    }
    */
    function submitForm(action)
    {   
        var email=document.getElementById( "user-result" ).value;
        alert(email);
        document.getElementById('optionForm').action = action;
    }

    // Date picker
    $(document).ready(function() {
      $('#datetimepicker1').datetimepicker(
                    
                    {   viewMode: 'years',
						format: 'YYYY-MM-DD',
						maxDate: '2009/12/31',
						minDate: '1938/01/01',
                        useCurrent: false,
  
                    })
                    .on('dp.hide', function (e) {
                    $('#optionForm').bootstrapValidator('revalidateField', 'dob');
   
                  });

      $('#optionForm').bootstrapValidator(
          {
            message: 'This value is not valid',
            live: 'enabled',
            fields:{
              dob: {
                    validators: {
                        notEmpty: {
                            message: 'Date of Birth is required'
                        }
                    }
                }
            },
             
            onSuccess: function(e, data) {
                        //this section before submit
              //document.getElementById('optionForm').submit();
            }
          }
        )
          
          .on('error.field.bv', function(e, data) {
          if (data.bv.getSubmitButton()) {
              data.bv.disableSubmitButtons(false);
          }
      })
      .on('success.field.bv', function(e, data) {
          if (data.bv.getSubmitButton()) {
              data.bv.disableSubmitButtons(false);
          }
      }).on('success.form.bv', function(e) {
        e.preventDefault(); // Prevent the form from submitting 
        
        $('#optionForm').bootstrapValidator('defaultSubmit');
      });
      });
</script>
    <script >
        $(window).load(function(){
            //Checking Unique Email//
                    
        $("#checkEmail").change(function(){  
           
            var email = $("#checkEmail").val();
            
            if(validateEmail(email)){
                check_email_ajax(email);
            }else if(email==""){
                $("#user-result").empty();
            }
            else{
                $("#user-result").html('<img src="images/ajax-loader.gif">');
                $("#user-result").html('กรุณากรอกอีเมลล์ให้ถูกต้อง');
                document.getElementById("user-result").style.color = "red";
            }

        }); 
           
           function validateEmail(email) {
                
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
               
               
        }

//          ("#username").keyup(function (e){
//            var email = $("#checkEmail").val();
//            check_email_ajax(email);
//        }); 
//        $

        function check_email_ajax(email){
            $("#user-result").html('<img src="images/ajax-loader.gif">');
  
             $.ajax({
                    type:"get",
                    url:"{{ url('/CheckEmail') }}",
                    data:
                        {email:email,
                        _token: '{{csrf_token()}}'},
           
                    success:function(data){

                        if(data==0){
                           document.getElementById("button-sub").disabled = false;
                            $("#user-result").html('สามารถใช้อีเมลล์นี้ได้');
                            document.getElementById("user-result").style.color = "green";

                        }

                        else{
                             
                            $("#user-result").html('อีเมลล์นี้ถูกใช้ไปแล้ว');
 
                           document.getElementById("user-result").style.color = "red";
                            
                        }
                    }
                 });

            }
 
           
});     
        function checkSubmit(){
//            alert("hi");
//        $.ajaxSetup({
//    headers: {
//        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//    }
//});
          $.when($.ajax(checkSubmiting())).then(function () {

            tester();

            });
       }
     
        function tester(){
            document.getElementById("button-sub").disabled = false;
        }
        
        function checkSubmiting(){
             var x = document.getElementById("user-result").innerHTML;
                
                    if( x=='อีเมลล์นี้ถูกใช้ไปแล้ว' || x=='กรุณากรอกอีเมลล์ให้ถูกต้อง'){
                        
                        document.getElementById("button-sub").disabled = true;
//                        $('#button-sub').addClass('disabled');
                        alert(x);
                    }
        }

    
</script>

    <!-- Mobile Specific Metas –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- FONT –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link href="https://fonts.googleapis.com/css?family=Kanit:300,300i,400,400i,500&amp;subset=thai" rel="stylesheet">

    <!-- CSS –––––––––––––––––––––––––––––––––––––––––––––––––– -->
    <link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/skeleton.css">
    <link rel="stylesheet" href="css/survey-custom.css">



</head>

<body>
            <div class="main-header">
        <div class="head-left">
            <img src="images/RR_tagline.png" onerror="this.onerror=null; this.src='images/RR_tagline.svg'" style="height: 43px;">
        </div>
        <div class="head-right">
            <img src="images/rrlogo.png" onerror="this.onerror=null; this.src='images/rrlogo.svg'" style="height: 40px;">
        </div>
        <div style="clear:both;"></div>
    </div>


          <main>
            @yield('content')
        </main>
</body>
</html>